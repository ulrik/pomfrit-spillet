# Frenchfries spillet

Et intro-forløb til app udvikling via [cordova][2] til brug i [Coding Pirates][3] regi.

Spillet er originalt udviklet af Tobias Eriksen fra [Mindthemedia][4], men vi har fået lov at bruge det i uddannelsesøjemed.

Opsætning af [npm][1], [cordova][2] skal være på plads før vi kan komme i gang, men når det er, så:

## Steps i at komme i gang

1. Klon projektet her.
2. Gå ind i mappen.
3. Klargør cordova projektet.
4. Prøv det!

```sh
:~$ git clone git@bitbucket.org:mindthemediateam/the-frenchfries-game.git
:~$ cd the-frenchfries-game
:~$ cordova prepare
:~$ cordova run browser -- --live-reload
```

## Mappestrukturen

```
+- hooks
+- node_modules
+- platforms
+- plugins
+- www
  +- css
  +- fonts
  +- img
  +- js
  +- index.html
```

### kort beskrevet

- [hooks][5] Her kan man hooke sig ind i de processer cordova kører gennem.
- [node_modules][6] Indeholder de javascript moduler der skal bruges i appen.
- [platforms][7] Indeholder platform specifikke filer, eks. hvis der skal bruges noget specielt til ios eller android.
- [plugins][8] Indeholder cordova plugins til at hjælpe med forskellige funktionaliteter.
- `www` Indeholder den kode vi kommer til at bygge på.
  - `css` mappen indeholder stylingen af appen.
  - `fonts` mappen er fonte vi kommer til at bruge - eks. [Pirata][9] fonten.
  - `img` mappen indeholder billeder der vil blive brugt i appen, primært app-ikoner.
  - `js` mappen indeholder app koden.
    - `app.js` er app koden, den der kommer til at indeholde al logikken til spillet.


[1]: https://nodejs.org/en/download/
[2]: https://cordova.apache.org/docs/en/latest/guide/cli/index.html
[3]: https://codingpirates.dk/
[4]: http://mindthemedia.com/
[5]: http://cordova.apache.org/docs/en/dev/guide/appdev/hooks/index.html
[6]: https://www.npmjs.com/
[7]: https://cordova.apache.org/docs/en/latest/reference/cordova-cli/index.html#cordova-platform-command
[8]: http://cordova.apache.org/plugins/
[9]: https://fonts.google.com/specimen/Pirata+One
